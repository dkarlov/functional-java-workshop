package com.griddynamics.workshop.model;

public enum Permission {
    ADD, EDIT, SEARCH, DELETE
}
