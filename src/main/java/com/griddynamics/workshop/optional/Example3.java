package com.griddynamics.workshop.optional;

import com.griddynamics.workshop.annotation.Good;
import com.griddynamics.workshop.annotation.Ugly;
import com.griddynamics.workshop.model.User;

import java.util.Optional;

public class Example3 {

    @Ugly
    class NullProtectionOverEngineering {
        public User copyUser(User user) {
            User copy = new User();

            Optional.ofNullable(user.getName())
                    .ifPresent(copy::setName);
            copy.setAge(user.getAge());
            return copy;
        }
    }

    @Good
    class SimpleConditionalCopying {
        public User copyUser(User user) {
            User copy = new User();

            if (user.getName() != null) {
                copy.setName(user.getName());
            }
            copy.setAge(user.getAge());
            return copy;
        }
    }
}
